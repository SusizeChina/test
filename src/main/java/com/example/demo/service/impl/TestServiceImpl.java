package com.example.demo.service.impl;

import com.example.demo.dao.ClassMapper;
import com.example.demo.dao.UserDao;
import com.example.demo.entity.Class;
import com.example.demo.entity.User;
import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ClassMapper classMapper;

    @Override
    public List<User> findAll(int id) {
        List<User> all = userDao.findAll(id);
        return all;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public boolean insert() {
        Class s=new Class();
        s.setClassId(2);
        s.setName("LIHUOQUAN");
        int insert = classMapper.insert(s);
        Class s1=new Class();
        s1.setClassId(2);
        s1.setName("LIHUOQUAN2SDSD");
        int insert1 = classMapper.insert(s1);
        return false;
    }
}
