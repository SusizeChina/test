package com.example.demo.service;

import com.example.demo.entity.User;

import java.util.List;

public interface TestService {

    /**
     * @param id
     * @return
     */
    List<User> findAll(int id);

    boolean insert();
}
