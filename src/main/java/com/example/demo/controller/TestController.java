package com.example.demo.controller;

import com.example.demo.dao.ClassMapper;
import com.example.demo.entity.Class;
import com.example.demo.entity.User;
import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TestController {


    @Autowired
    private TestService testService;

    @Value("${demo.test.name}")
    private String name;

    @Value("${demo.test.age}")
    private String age;

    @RequestMapping("/test")
    public String test(String id) {
        System.out.println(age);
        return name;
    }

    @RequestMapping("/find_all")
    @ResponseBody
    public List<User> findAll(int id) {
        List<User> users = testService.findAll(id);
        return users;
    }

    @GetMapping("/insert")
    @ResponseBody
    public void insert() {
       testService.insert();
    }
}
